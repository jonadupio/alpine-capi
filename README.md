# Alpine-capi: Docker image with Capistrano

[![build status](https://gitlab.com/jonathan.dupuich/alpine-capi/badges/master/build.svg)](https://gitlab.com/jonathan.dupuich/alpine-capi/commits/master)

Alpine-capi is a Docker image, based on [Alpine](https://hub.docker.com/_/alpine/), with the last version of [Capistrano](http://capistranorb.com/).

## How to use this image

```
docker run -it --rm -v `pwd`:/root/cap/ registry.gitlab.com/jonathan.dupuich/alpine-capi /bin/bash
```
